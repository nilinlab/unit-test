#!/bin/dash
mkdir -p ~/projects/stam/L03/unit-test
code ~/projects/stam/L03/unit-test

git init
npm init -y

npm i --save express
npm i --save-dev mocha chai

echo "node_modules" > .gitignore

# git remote add origin https://gitlab.com/{username}/unit-test
# git remote set-url origin
# git push -u origin main

touch README.md
mkdir src test
touch src/main.js src/calc.js
mkdir public
touch public/index.html
touch test/calc.test.js

# test files that go to the staging area
git add . --dry-run